# Seminario de Videojuegos con herramientas libres

![Cartel del evento](img/featured.png)

Taller dedicado a aprender diseño y desarrollo de videojuegos usando
herramientas libres. Comenzaremos con programación de juegos con Common LISP
y dedicaremos la última sesión de cada mes a abordar temas como música, motores
libres, herramientas para creación de arte (pixel art, voxel art, etc),
narrativa y el concepto de juego.


